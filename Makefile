# Daniel Joffe
# u03dj17

DEBUG := -O0 -g3 -ggdb -fsanitize=address,undefined
RELEASE := -Os -O2 -DNDEBUG

CC := gcc
CFLAGS := -Wall -Wextra --pedantic
CFLAGS += $(RELEASE)
CFLAGS += -std=gnu11


all: shell

shell: shell.o mymemory.o util.o
	$(CC) $(CFLAGS) -o shell shell.o mymemory.o util.o

shell.o: shell.c mymemory.h
	$(CC) $(CFLAGS) -c shell.c

mymemory.o: mymemory.c mymemory.h util.h
	$(CC) $(CFLAGS) -c mymemory.c

util.o: util.c util.h
	$(CC) $(CFLAGS) -c util.c

clean:
	rm *.o
