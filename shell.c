/* 
 * Daniel Joffe
 * u03dj17
*/

/* shell.c
 * Guided tour of the functionality provided by the mymemory library.
*/ 

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <assert.h>

#include "mymemory.h"
#include "util.h"

void test_strings(void);
void test_alloc_many(void);
void test_alloc_large(void);
void test_defrag(void);

void wait_for_user(void);

int main(void)
{
    printf("shell> Initialising.\n");
    wait_for_user();
    
    initialize();
    
    print_memory();
    print_segment_table();
    
    test_strings();
    test_alloc_many();
    test_alloc_large();
    test_defrag();
    
    printf("shell> end\n");
}

void test_strings(void)
{
    printf("shell> Allocating some strings.\n");
    wait_for_user();
    
    char *src = "this test";
    char *str1 = mymalloc(strlen(src) + 1);
    strcpy(str1, src);
    printf("shell> Content of allocated memory (str1): \"%s\"\n", str1);
    
    src = "test no.\ttwo";
    char *str2 = mymalloc(strlen(src));
    strcpy(str2, src);
    printf("shell> Content of allocated memory (str2): \"%s\"\n", str2);
    
    wait_for_user();
    
    print_memory();
    print_segment_table();
    
    printf("shell> Freeing str1.\n");
    wait_for_user();
    
    myfree(str1);
    
    print_memory();
    print_segment_table();
    
    printf("shell> Freeing str2.\n");
    wait_for_user();
    
    myfree(str2);
    
    print_memory();
    print_segment_table();
}

void test_alloc_many(void)
{
    printf("shell> Testing large number of allocations.\n");
    wait_for_user();
    
    int *ptrs[1000];
    size_t n_alloc = 0;
    
    while (
        n_alloc < 1000 &&
        (ptrs[n_alloc] = mymalloc(sizeof(int))) != NULL
    ) {
        *ptrs[n_alloc] = (int) n_alloc;
        n_alloc++;
    }
    printf("shell> Allocated %zu integers successfully.\n", n_alloc);
    wait_for_user();
    
    print_memory();
    print_segment_table();
    
    printf("shell> Freeing.\n");
    wait_for_user();
    
    for (size_t i = 0; i < n_alloc; i++) {
        myfree(ptrs[i]);
    }
    
    print_memory();
    print_segment_table();
    
    printf("shell> Note that there are many free segments that could be merged.\n");
    wait_for_user();
}

void test_alloc_large(void)
{
    printf("shell> Testing large allocation.\n");
    wait_for_user();
    
    size_t alloc_size = 1;
    void *ptr;
    
    while ((ptr = mymalloc(alloc_size)) != NULL) {
        print_segment_table();
        printf("shell> Allocated %zu bytes successfully.\n", alloc_size);
        wait_for_user();
        
        myfree(ptr);
        alloc_size *= 2;
    }
    printf("shell> Failed at allocating %zu bytes\n", alloc_size);
    wait_for_user();
}

void test_defrag(void)
{
    srand(0); // change to time(NULL) to live on the wild side.
    
    printf("shell> Testing Defragmentation\n");
    wait_for_user();
    
    printf("shell> Allocating\n");
    wait_for_user();
    // Get a bunch of pointers.
    char *strings[6];
    for (int i = 0; i < 6; i++) {
        strings[i] = mymalloc(40);
        sprintf(strings[i], "%d", i * 100);
    }
    
    // The pointer list need not be sorted in any fashion.
    shuffle(strings, 6, sizeof(char *));
    
    print_memory();
    print_segment_table();
    
    printf("shell> Allocated memory:\n");
    for (int i = 0; i < 6; i++) {
        printf("%p: \"%s\"\n", strings[i], strings[i]);
    }
    wait_for_user();
    
    printf("shell> Freeing.\n");
    wait_for_user();
    
    for (int i = 3; i < 6; i++) {
        myfree(strings[i]);
    }
    
    print_memory();
    print_segment_table();
    printf("shell> Allocated memory:\n");
    for (int i = 0; i < 3; i++) {
        printf("%p:\t\"%s\"\n", strings[i], strings[i]);
    }
    wait_for_user();
    
    printf("shell> Defragmenting\n");
    wait_for_user();
    
    mydefrag((void **) strings);
    
    print_memory();
    print_segment_table();
    printf("shell> Allocated memory:\n");
    for (int i = 0; i < 3; i++) {
        printf("%p:\t\"%s\"\n", strings[i], strings[i]);
    }
    wait_for_user();
    
    // Free the remaining strings.
    for (int i = 0; i < 3; i++) {
        myfree(strings[i]);
    }
}

void wait_for_user(void) {
    printf("Press enter to continue...\n");
    int c;
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}

