# Sytle Guide

### Line Length

Ideally 72 or less; 80 is a hard limit.

### Indentation

One indent for a block of code is exactly 4 spaces.

### Header File Structure
```
#ifndef HEADER_NAME_H
#define HEADER_NAME_H

macro definitions

type definitions

function declarations

#endif HEADER_NAME_H
```

### Source File Structure
```
#include "header with functions to implement.h"

#include <standard header files.h>

#include <nonstandard header files.h>

#include "custom header files.h"


macro definitions


type definitions


global variable declarations


static function declarations


function implementations


static function implementations
```
### Function Declarations

    type function_name(type params, ...);
    type long_function_name(
        type long_parameter_name,
        ...
    );
    type no_params(void);

### Function Definitions
```
    type function_name(type params, ...)
    {
        ...
    }
```
```
type long_function_name(
    type long_parameter_name,
    ...
)
{
    ...
}
```
```
type no_params(void)
{
    ...
}
```
### Function Calls
```
variable = func(params);
```
```
variable = long_function_name(
    long_parameter_name,
    another_long_parameter_that_is_really_a_function_call(),
    final_param
);
```
### Pointer Declarations
```
type *ptr;
```
```
type *ptr = initialiser;
```

### Loops
```
while (condition) {
    ...
}
```
```
do {
    ...
}
```
```
for (init; cond; next) {
    ...
}
```
```
for (
    long_initialisation_expression;
    long_expression_deciding_whether_to_keep_looping;
    long_expression_moving_onto_the_next_iteration
) {
    ...
}
```
### Assertions
```
    assert(self-explanatory precondition);
    assert(cryptic precondition && "explanation");

    code_logic
```
