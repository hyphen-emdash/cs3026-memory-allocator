/* 
 * Daniel Joffe
 * u03dj17
*/

/* util.h
 * A bunch of miscellaneous functions and macros for general use
 */

#ifndef UTIL_H
#define UTIL_H

#include <stddef.h>
#include <assert.h>

// Unsafe macros - do not use on expressions with side-effects.
//#define CEIL_DIV(a, b) ( ((a) + (b) - 1) / (b) )
#define CEIL_DIV(a, b) ( (a)/(b) + (a % b != 0) )

#define MIN(a, b) ( (a < b)? (a) : (b) )


void hexdump(const void *p, size_t bytes, size_t line_length);

void memswp(void *a, void *b, size_t n);

void shuffle(void *base, size_t n_elt, size_t size);

// Linear search on array of pointers.
void **findptr(void *key, void **ptrs, size_t n);

#endif // UTIL_H
