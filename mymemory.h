/* 
 * Daniel Joffe
 * u03dj17
*/

/* mymemory.h
 * 
 * Describes structures for memory management.
 */

#ifndef MYMEMORY_H
#define MYMEMORY_H

#include <stdint.h>
#include <stddef.h>


// Interface for main functionality.

// Call this exactly once in your program, before you use mymalloc
// or myfree
void initialize(void);

// Allocates and returns a block of memory with at least size bytes.
void *mymalloc(size_t size);

// Deallocates a block of memory.
void myfree(void *);

// Moves the memory owned by each pointer, and changes the pointers
// accordingly. eg, If *(int *) ptrs[i] == 6 before the call,
// *(int *) ptrs[i] == 6 after the call still, even if ptrs[i] has
// changed.
// You must pass a list of ALL the unfreed pointers you have received
// from mymalloc.
// Does not work sensibly when the memory allocated by mymalloc
// references other memory allocated by mymalloc - eg a linked list.
void mydefrag(void **ptrlist);

void print_memory(void);
void print_segment_table(void);

#endif // MYMEMORY_H
