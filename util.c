/* 
 * Daniel Joffe
 * u03dj17
*/

#include "util.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

// Returns the number of bytes from p that were printed.
static void print_hex(const unsigned char *p, size_t n, size_t lim);
static void print_chars(const unsigned char *p, size_t n, size_t lim);

// Give a buffer of at least 3 chars long - one of which is for '\0'.
static void char_represent(char c, char *out);

static int hex_digits(size_t n);

void hexdump(const void *p, size_t bytes, size_t line_length)
{
    const unsigned char *base = p;
    
    size_t i = 0;
    while (i != bytes) {
        assert(i < bytes);
        
        size_t to_print = MIN(bytes - i, line_length);
        
        // Print address offset.
        printf("[%0*zX] |", hex_digits(bytes-1), i);
        
        print_hex(base + i, to_print, line_length);
        printf(" | ");
        print_chars(base + i, to_print, line_length);
        
        putchar('\n');
        
        i += to_print;
    }
}

void memswp(void *a, void *b, size_t n)
{
    if (n > 0) {
        assert(a != NULL);
        assert(b != NULL);
    }
    
    unsigned char *ap = a;
    unsigned char *bp = b;
    
    for (size_t i = 0; i < n; i++) {
        unsigned char temp = ap[i];
        ap[i] = bp[i];
        bp[i] = temp;
    }
}

void shuffle(void *base, size_t n_elt, size_t size)
{
    if (size > 0) {
        assert(base != NULL);
    }
    
    unsigned char *basep = base;
    
    for (size_t i = 0; i < n_elt; i++) {
        size_t j = rand() % n_elt;
        memswp(basep + i*size, basep + j*size, size); 
    }
}

void **findptr(void *key, void **ptrs, size_t n)
{
    for (size_t i = 0; i < n; i++) {
        if (key == ptrs[i]) {
            return ptrs + i;
        }
    }
    return NULL;
}


static void print_hex(const unsigned char *p, size_t n, size_t lim)
{
    assert(p != NULL);
    
    for (size_t i = 0; i < lim; i++) {
        if (i < n) {
            printf(" %02x", p[i]);
        }
        else {
            printf("   ");
        }
    }
}

static void print_chars(const unsigned char *p, size_t n, size_t lim)
{
    assert(p != NULL);
    
    for (size_t i = 0; i < lim; i++) {
        if (i < n) {
            char out[3];
            char_represent(p[i], out);
            printf("%2s ", out);
        }
        else {
            printf("   ");
        }
    }
}

static void char_represent(char c, char *out)
{
    assert(out != NULL);
    
    if (isgraph(c)) {
        sprintf(out, " %c", c);
    }
    else {
        char *src;
        switch (c) {
            case ' ':
                src = "  ";
                break;
            case '\a':
                src = "\\a";
                break;
            case '\b':
                src = "\\b";
                break;
            case '\n':
                src = "\\n";
                break;
            case '\r':
                src = "\\r";
                break;
            case '\t':
                src = "\\t";
                break;
            case '\v':
                src = "\\v";
                break;
            default:
                src = "::";
                break;
        }
        
        strcpy(out, src);
    }
}

static int hex_digits(size_t n)
{
    int ret = 0;
    do {
        ret++;
        n /= 0x10;
    } while (n > 0);
    return ret;
}
