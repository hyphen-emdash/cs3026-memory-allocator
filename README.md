# OS Assignment 1 - Memory Management

## How To Run

    make
    ./shell

## What to Expect

First, the shell allocates and frees some strings, printing the segment table and heap to show everything is valid.

Then it tests the limits of the allocater, requesting many small chunks of memory and one large chunk.

Finally, it shows off defragmentation by allocating 6 strings and freeing half of them randomly to create some fragmentation; and then defragmenting, showing that they are all together at the end.

## Implemtation Design Decisions

+ I moved some data-structure-definitions from `mymemory.h` to `mymemory.c`, as it's not necessary for the user of the library to know about them. Likewise for a couple of global variables.

+ I want my code to be portable, and that means respecting  alignment restrictions. I don't know the alignment restrictions are on my own machine, let alone yours, so I just try to respect them all. That means storing my 'heap' as an array of blocks, where each block is extremely restricted in alignment.
  On my machine, each block is 32 bytes. (That could probably be reduced to 8, but I don't want to make unsubstantiated assumptions.)
  
  Drawbacks:
  
  * Internal fragmentation of memory for small allocations.
  
  Benefits:
  
  * Not going into undefined behaviour.

+ My segment table is an array held in static memory, not a dynamically-allocated linked list.
  
  Benefits:
  
  * Simpler logic.
  * More reliable, as there is no allocation that can fail.
  * Takes less space in the worst case, as it doesn't have to store `next` pointers, or any of the memory overhead that comes with `malloc`. Though I am using space for segment descriptors I don't need, a linked-list implementation would need to store the same number in the worst case scenario. I'd rather find out that my machine isn't capable of handling that when I start my program than halfway through its execution.
  * Better cache performance.
  * I'm implementing `malloc`; it feels like cheating to use `malloc`.
  
  Drawbacks:
  
  * None that I can think of.

+ In regards to segment management, when I free a segment I simply mark it as unused. This can cause external fragmentation when there are two adjacent unused segments - they cannot be used to allocate one large object. To counteract this, if `mymalloc` fails to find a free segment, it merges all the unused segments and looks again.

## Interface Design Decisions

+ Regarding initialisation for `malloc`, I take the same approach as the C standard  - not my problem. If someone `malloc`s memory it is their responsibility to write to it before trying to read from it. I am aware this can lead to security bugs in poorly-written programs. If the user of this library doesn't want security bugs, they should write their program properly.


