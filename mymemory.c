/* 
 * Daniel Joffe
 * u03dj17
*/

#include "mymemory.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <complex.h>
#include <assert.h>

#include <unistd.h>

#include "util.h"

#define MAXMEM (1024)
#define MAX_BLOCKS (CEIL_DIV(MAXMEM, sizeof(block)))

typedef union {
    char a;
    short b;
    int c;
    long d;
    long long e;
    float f;
    double g;
    long double h;
    complex float i;
    complex double j;
    complex long double k;
    void *l;
    void (*m)();
    intmax_t n;
} block;

struct segment {
    block *start;
    size_t n_blocks;
    bool in_use;
};

// Our 'heap'.
// Automatically initialised to 0, but it's not our responsibility to
// initialise it anyway.
static block heap[MAX_BLOCKS];

// Our pool of segment descriptors.
// If i < used_segments, segments[i] is defined.
// Generally not kept in any kind of order.
static struct segment segment_table[MAX_BLOCKS];
// Total number of segments.
static size_t n_segments = 0;
// Number of segments that the user has valid references to.
static size_t n_in_use = 0;


// Find a free segment large enough.
static struct segment *find_free(
    struct segment *seg_tab,
    size_t seg_count,
    size_t blocks_needed
);

// Find the segment responsible for a given pointer. Returns NULL if
// not found.
static struct segment *segment_for(
    struct segment *seg_tab,
    size_t seg_count,
    const void *ptr
);

// If a segment has n + m blocks, breaks it into segments of n and m
// blocks, and keeps both in the segment table.
// If the given segment already has exactly n blocks, does nothing.
// No guarantees if the segment has fewer than n blocks.
static struct segment *break_segment(struct segment *, size_t n);

// Returns whether or not a segment is suitable to used for allocating
// a certain number of blocks.
static inline bool segment_works(struct segment, size_t blocks_needed);

// Merges all adjacent unused segments.
static void merge_unused_segments(
    struct segment *seg_tab,
    size_t *seg_count
);

// Compares two segments based on the pointers they are responsible for.
// To be passed to qsort.
static int segment_cmp(const void *, const void *);

static void print_segment(struct segment);


void initialize(void)
{
    extern block heap[MAX_BLOCKS];
    extern struct segment segment_table[MAX_BLOCKS];
    extern size_t n_segments;
    
    assert(n_segments == 0 && "double initialize");
    
    segment_table[0] = (struct segment) {
        .start = heap,
        .n_blocks = MAX_BLOCKS,
        .in_use = false,
    };
    n_segments = 1;
}

void print_memory(void)
{
    extern block heap[MAX_BLOCKS];
    hexdump(heap, sizeof(heap), 8);
}

void print_segment_table(void)
{
    extern struct segment segment_table[MAX_BLOCKS];
    extern size_t n_segments;
    extern size_t n_in_use;
    
    printf("Segment table[%zu used / %zu total]: {\n", n_in_use, n_segments);
    for (size_t i = 0; i < n_segments; i++) {
        print_segment(segment_table[i]);
    }
    printf("}\n");
}

void *mymalloc(size_t size)
{
    if (size == 0) {
        return NULL;
    }
    
    extern struct segment segment_table[MAX_BLOCKS];
    extern size_t n_segments;
    extern size_t n_in_use;
    
    assert(n_segments <= MAX_BLOCKS);
    
    size_t n_blocks = CEIL_DIV(size, sizeof(block));
    
    // Look for a free segment large enough.
    struct segment *free_segment = find_free(
        segment_table,
        n_segments,
        n_blocks
    );
    if (free_segment == NULL) {
        // If we couldn't find room, clean up and try again.
        merge_unused_segments(segment_table, &n_segments);
        
        free_segment = find_free(
            segment_table,
            n_segments,
            n_blocks
        );
        if (free_segment == NULL) {
            // If there truly was no room.
            return NULL;
        }
    }
    
    assert(free_segment->n_blocks >= n_blocks);
    
    // Break the segment in two to save space, and return one of them.
    break_segment(free_segment, n_blocks);
    free_segment->in_use = true;
    n_in_use++;
    
    return free_segment->start;
}

void myfree(void *ptr)
{
    if (ptr == NULL) {
        return;
    }
    
    extern struct segment segment_table[MAX_BLOCKS];
    extern size_t n_segments;
    extern size_t n_in_use;
    
    struct segment *s = segment_for(segment_table, n_segments, ptr);
    
    assert(s != NULL && "Could not find segment to free");
    
    s->in_use = false;
    n_in_use--;
}

void mydefrag(void **ptrlist)
{
    assert(ptrlist != NULL);
    
    extern block heap[MAX_BLOCKS];
    extern struct segment segment_table[MAX_BLOCKS];
    extern size_t n_segments;
    extern size_t n_in_use;
    
    assert(n_segments <= MAX_BLOCKS);
    assert(n_in_use <= MAX_BLOCKS);
    
    if (find_free(segment_table, n_segments, 1) == NULL) {
        // If there is no free space, defragging is meaningless.
        return;
    }
    
    merge_unused_segments(segment_table, &n_segments);
    
    // Move each segment in turn.
    block *cursor = heap;
    size_t moved = 0;
    for (size_t i = 0; moved < n_in_use; i++) {
        assert(cursor < heap + MAX_BLOCKS && "cursor out of bounds");
        
        if (!segment_table[i].in_use) {
            continue;
        }
        
        void **usrptr = findptr(segment_table[i].start, ptrlist, n_in_use);
        
        // Move memory associated with the segment.
        memmove(
            cursor,
            segment_table[i].start,
            segment_table[i].n_blocks * sizeof(block)
        );
        *usrptr = segment_table[i].start = cursor;
        cursor += segment_table[i].n_blocks;
        
        // Move over the segment descriptor.
        memmove(
            segment_table + moved,
            segment_table + i,
            sizeof(struct segment)
        );
        moved++;
    }
    // Make sure the rest of the segment table makes sense.
    n_segments = n_in_use;
    size_t blocks_used = cursor - heap;
    size_t blocks_free = MAX_BLOCKS - blocks_used;
    segment_table[n_segments] = (struct segment) {
        .start = cursor,
        .n_blocks = blocks_free,
        .in_use = false
    };
    n_segments++;
}

static struct segment *find_free(
    struct segment *seg_tab,
    size_t n_seg,
    size_t blocks_needed
)
{
    assert(n_seg <= MAX_BLOCKS);
    
    for (size_t i = 0; i < n_seg; i++) {
        if (segment_works(seg_tab[i], blocks_needed)) {
            return seg_tab + i;
        }
    }
    
    return NULL;
}

static inline bool segment_works(struct segment s, size_t blocks_needed)
{
    return (!s.in_use && s.n_blocks >= blocks_needed);
}

static struct segment *segment_for(
    struct segment *seg_tab,
    size_t n_seg,
    const void *ptr
)
{
    assert(seg_tab != NULL);
    
    for (size_t i = 0; i < n_seg; i++) {
        if (seg_tab[i].start == ptr) {
            return seg_tab + i;
        }
    }
    
    return NULL;
}

static struct segment *break_segment(struct segment *s, size_t n)
{
    assert(s != NULL);
    assert(
        s->n_blocks >= n &&
        "Breaking a segment into a larger chunk than itself."
    );
    
    extern struct segment segment_table[MAX_BLOCKS];
    extern size_t n_segments;
    
    if (n == s->n_blocks) {
        return s;
    }
    
    assert(n_segments < MAX_BLOCKS && "Too many segments.");
    
    segment_table[n_segments] = (struct segment) {
        .start = s->start + n,
        .n_blocks = s->n_blocks - n,
        .in_use = false,
    };
    n_segments++;
    
    s->n_blocks = n;
    
    return s;
}

static void merge_unused_segments(
    struct segment *seg_tab,
    size_t *n_seg
)
{
    assert(seg_tab != NULL);
    assert(n_seg != NULL);
    
    if (*n_seg < 2) {
        // Nothing to merge.
        return;
    }
    
    // Put adjacent segments next to each other in the table.
    qsort(seg_tab, *n_seg, sizeof(struct segment), segment_cmp);
    
    // This could be optimized by searching for long runs of free
    // segments rather than just two adjacent ones.
    size_t i = 0;
    while (i < *n_seg - 1) {
        if (!seg_tab[i].in_use && !seg_tab[i+1].in_use) {
            // If two adjacent segments are unused, merge them and don't
            // advance in the loop.
            seg_tab[i].n_blocks += seg_tab[i+1].n_blocks;
            
            // Move the rest of the segment descriptors along.
            memmove(
                seg_tab + i + 1,
                seg_tab + i + 2,
                (*n_seg - i - 2) * sizeof(struct segment)
            );
            (*n_seg)--;
        }
        else {
            i++;
        }
    }
}

void print_segment(struct segment seg)
{
    extern block heap[MAX_BLOCKS];
    
    printf("    Segment {\n");
    printf("        allocated:   %s\n", seg.in_use? "TRUE" : "FALSE");
    printf(
        "        start:       %p = base + 0x%zX bytes\n",
        (const void *) seg.start,
        (seg.start - heap) * sizeof(block)
    );
    printf("        blocks used: %zu\n", seg.n_blocks);
    printf("    }\n");
}

static int segment_cmp(const void *a, const void *b) {
    assert(a != NULL);
    assert(b != NULL);
    
    const struct segment *a_seg = a;
    const struct segment *b_seg = b;
    
    return a_seg->start - b_seg->start;
}
